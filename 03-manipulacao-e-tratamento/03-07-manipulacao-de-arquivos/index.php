<?php
require __DIR__ . '/../../fullstackphp/fsphp.php';
fullStackPHPClassName("03.07 - Manipulação de arquivos");

/*
 * [ verificação de arquivos ] file_exists | is_file | is_dir
 */
fullStackPHPClassSession("verificação", __LINE__);

$file = __DIR__ . "/file.txt";

if (file_exists($file) && is_file($file)) {
    echo "<p>Existe</p>";
} else {
    echo "<p>Não existe</p>";
}

var_dump($file);

/*
 * [ leitura e gravação ] fopen | fwrite | fclose | file
 */
fullStackPHPClassSession("leitura e gravação", __LINE__);

if (!file_exists($file) && !is_file($file)) {
    //crio o arquivo .txt
    $fileOpen = fopen($file, "w");

    // escrevo no arquivo .txt
    fwrite($fileOpen, "Linha 01" . PHP_EOL);
    fwrite($fileOpen, "Linha 02" . PHP_EOL);
    fwrite($fileOpen, "Lorem Ipsum is simply dummy text of the printing and typesetting industry." . PHP_EOL);

    // Fecho o arquivo
    fclose($fileOpen);
} else {
    var_dump(
        file($file),
        pathinfo($file)
    );
}

echo "<p>" . file($file)[2] . "</p>";

$openFile = fopen($file, "r");
while (!feof($openFile)) {
    echo "<p> " . fgets($openFile) . "</p>";
}

fclose($openFile);

/*
 * [ get, put content ] file_get_contents | file_put_contents
 */
fullStackPHPClassSession("get, put content", __LINE__);

// file_get_contents() = Lê o arquivo inteiro em uma string
// file_put_contents() = Grave uma string em um arquivo
// unlink() = Exclui o arquivo

$getContents = __DIR__ . "/teste2.txt";

if (file_exists($getContents) && is_file($getContents)) {
    echo file_get_contents($getContents);
} else {
    $data = "<article><h1>Francisco Assis</h1><p>CEO Web-Fr<br>contato@webfr.com.br</p></article>";
    file_put_contents($getContents, $data);

    echo file_get_contents($getContents);
}

// Deletando os arquivos
// unlink($getContents);
// unlink($file);

if (file_exists(__DIR__ . "/teste3.txt") && is_file(__DIR__ . "/teste3.txt")) {
    unlink(__DIR__ . "/teste3.txt");
}
