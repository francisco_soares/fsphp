<?php
require __DIR__ . '/../../fullstackphp/fsphp.php';
fullStackPHPClassName("03.04 - Manipulação de objetos");

/*
 * [ manipulação ] http://php.net/manual/pt_BR/language.types.object.php
 */
fullStackPHPClassSession("manipulação", __LINE__);

$arrProfile = [
    "name" => "Francisco Assis",
    "company" => "Agência Digital Web-Fr",
    "mail" => "francisco@webfr.com.br"
];

$objProfile = (object)$arrProfile;

var_dump(
    $arrProfile,
    $objProfile,
);

echo "<p>{$arrProfile['name']} trabalha na {$arrProfile['company']}</p>";
echo "<p>{$objProfile->name} trabalha na {$objProfile->company}</p>";

$ceo = $objProfile;
unset($ceo->company);
var_dump($ceo);

$company = new stdClass();
$company->company = "Agência Digital Web-Fr";
$company->ceo = $ceo;
$company->manager = new stdClass();
$company->manager->name = "Isa Basto";
$company->manager->mail = "isa@webfr.com.br";

var_dump($company);


/**
 * [ análise ] class | objetcs | instances
 */
fullStackPHPClassSession("análise", __LINE__);

$date = new DateTime();

var_dump([
    "date" => $date,
    "class" => get_class($date),
    "methods" => get_class_methods($date),
    "vars" => get_object_vars($date),
    "parent" => get_parent_class($date),
    "subclass" => is_subclass_of($date, "DateTime")
]);

$exceptionPdo = new PDOException();

var_dump([
    "class" => get_class($exceptionPdo),
    "methods" => get_class_methods($exceptionPdo),
    "vars" => get_object_vars($exceptionPdo),
    "parent" => get_parent_class($exceptionPdo),
    "subclass" => is_subclass_of($exceptionPdo, "Exception")

]);
