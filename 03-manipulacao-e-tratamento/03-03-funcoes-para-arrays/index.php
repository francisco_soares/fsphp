<?php
require __DIR__ . '/../../fullstackphp/fsphp.php';
fullStackPHPClassName("03.03 - Funções para arrays");

/*
 * [ criação ] https://php.net/manual/pt_BR/ref.array.php
 */
fullStackPHPClassSession("manipulação", __LINE__);

$index = [
    "Mr. Big",
    "Deff Leppard",
    "HardLine"
];

$assoc = [
    "band_1" => "Mr. Big",
    "band_2" => "Deff Leppard",
    "band_3" => "HardLine"
];

// [array_unshift] Inserindo indices no começo do array
array_unshift($index, "Road Rash", "Saxon");
$assoc = ["band_4" => "Road Rash", "band_5" => "Saxon"] + $assoc;

// [array_push] Inserindo indices no fim do array
array_push($index, "");
$assoc = $assoc + ["band_6" => ""];


// [array_shift] Remover o primeiro indice do array
array_shift($index);
array_shift($assoc);

// [array_pop] Remover o último indice do array
array_pop($index);
array_pop($assoc);

// [array_filter] Elimina qualquer indice que não tem valor
array_unshift($index, ""); // usando apenas para exemplificar.
$assoc = $assoc + ["band_7" => ""];

$index = array_filter($index);
$assoc = array_filter($assoc);

var_dump(
    $index,
    $assoc
);

/*
 * [ ordenação ] reverse | asort | ksort | sort
 */
fullStackPHPClassSession("ordenação", __LINE__);

// [array_reverse] - Retorna uma matriz com elementos em ordem reversa
$index = array_reverse($index);
$assoc = array_reverse($assoc);

// [asort] - Retorna os VALORES do array em ordem alfabetica
asort($index);
asort($assoc);

// [ksort] - Retorna os INDICES do array em Crescente
ksort($index);

// [krsort] - Retorna os VALORES do array em Decrescente
krsort($index);

// [sort] - Ordena os INDICES E VALORES do array em Crescente
sort($index);

// [rsort] - Ordena os INDICES E VALORES do array em Decrescente
rsort($index);

var_dump(
    $index,
    $assoc
);

/*
 * [ verificação ]  keys | values | in | explode
 */
fullStackPHPClassSession("verificação", __LINE__);


var_dump([
    array_keys($assoc), // Verifica se tem o indice
    array_values($assoc), // Verifica se tem o valores
]);

// Verifica se tem o valor o chave no array
if(in_array("Deff Leppard", $assoc)){
    echo "<p>Photograf</p>";
}

$arrToString = implode(", ", $assoc);
echo "<p>Eu curto muito {$arrToString} e outras Bandas!</p>";
echo "<p>{$arrToString}</p>";

var_dump(explode(", ", $arrToString));

/**
 * [ exemplo prático ] um template view | implode
 */
fullStackPHPClassSession("exemplo prático", __LINE__);

$profile = [
    "name" => "Francisco Assis",
    "company" => "Web-Fr",
    "mail" => "francisco@webfr.com.br",
];

$template = <<<TPL
    <article>
        <h1>{{name}}</h1>
        <p>{{company}}<br>
        <a href="mailto:{{mail}}" title="Enviar e-mail para {{mail}}">Enviar e-mail</a>
        </p>
    </article>
TPL;

echo $template;

echo str_replace(
    array_keys($profile), array_values($profile), $template
);

$replaces = "{{" . implode("}}&{{", array_keys($profile)) . "}}";

echo str_replace(
    explode("&", $replaces),
    array_values($profile),
    $template
);
