<?php
require __DIR__ . '/../../fullstackphp/fsphp.php';
fullStackPHPClassName("03.10 - Upload de arquivos");

/*
 * [ upload ] sizes | move uploaded | url validation
 * [ upload errors ] http://php.net/manual/pt_BR/features.file-upload.errors.php
 */
fullStackPHPClassSession("upload", __LINE__);

/* Checando antes de criar a pasta UPLOADS */
$folder = __DIR__ . "/uploads";
if (!file_exists($folder) || !is_dir($folder)) {
    mkdir($folder, 0755);
}

/* Verificando o tamanho que o servidor me permite enviar pelo formulário */
var_dump([
    "filesize" => ini_get("upload_max_filesize"),
    "postsize" => ini_get("post_max_size")
]);

/* Verificando a extensão de um arquivo que será enviado */
var_dump([
    filetype(__DIR__ . "/index.php"),
    mime_content_type(__DIR__ . "/index.php")
]);

/* Tratando para envio de arquivos com extensões especificas. */
$getPost = filter_input(INPUT_GET, "post", FILTER_VALIDATE_BOOLEAN);

if ($_FILES && !empty($_FILES['file']['name'])) {
    $fileUpload = $_FILES['file'];
    var_dump($fileUpload);

    $allowedTypes = [
        "image/jpg",
        "image/jpeg",
        "image/png",
        "application/pdf"
    ];

    /* Altero o nome do arquivo para TIME() */
    $newFileName = time() . mb_strstr($fileUpload['name'], ".");


    if (in_array($fileUpload['type'], $allowedTypes)) {


        if (move_uploaded_file($fileUpload['tmp_name'], __DIR__ . "/uploads/{$newFileName}")) {
            echo "<p class='trigger accept'>Arquivo enviado com sucesso!</strong></p>";
        } else {
            echo "<p class='trigger error'>Erro inesperado</p>";
        }
    } else {
        echo "<p class='trigger error'>Tipo de arquivo não é permitido, envie arquivos no formato <strong>JPG, JPEG, PNG ou PDF</strong></p>";
    }
} elseif ($getPost) {
    echo "<p class='trigger warning'>Whoops, para que o arquivo é muito grande!</p>";
} else {
    if ($_FILES) {
        echo "<p class='trigger warning'>Selecione um arquivo antes de enviar</p>";
    }
}

include __DIR__ . "/form.php";
var_dump(scandir(__DIR__ . "/uploads"));
