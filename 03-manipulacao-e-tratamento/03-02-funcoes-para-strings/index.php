<?php
require __DIR__ . '/../../fullstackphp/fsphp.php';
fullStackPHPClassName("03.02 - Funções para strings");

/*
 * [ strings e multibyte ] https://php.net/manual/en/ref.mbstring.php
 */
fullStackPHPClassSession("strings e multibyte", __LINE__);

$string = "O último show do AC/DC foi incrível!";

var_dump([
    "string" => $string,
    "strlen" => strlen($string),
    "mb_strlen" => mb_strlen($string),
    "substr" => substr($string, 14),
    "mb_substr" => mb_substr($string, 14),
    "strtoupper" => strtoupper($string),
    "mb_strtoupper" => mb_strtoupper($string),
]);

/**
 * [ conversão de caixa ] https://php.net/manual/en/function.mb-convert-case.php
 */
fullStackPHPClassSession("conversão de caixa", __LINE__);

$mbString = $string;

var_dump([
    "mb_strtoupper" => mb_strtoupper($string),
    "mb_strtolowwer" => mb_strtolower($string),
    "mb_convert_case UPPER" => mb_convert_case($mbString, CASE_UPPER),
    "mb_convert_case LOWER" => mb_convert_case($mbString, CASE_LOWER),
    "mb_convert_case TITLE" => mb_convert_case($mbString, MB_CASE_TITLE),
]);


/**
 * [ substituição ] multibyte and replace
 */
fullStackPHPClassSession("substituição", __LINE__);

$mbReplace = $mbString . " Fui, iria novamente, e foi épico";

var_dump([
    "mb_strlen" => mb_strlen($mbReplace),
    "mb_strpos" => mb_strpos($mbReplace, ", "),
    "mb_strpos" => mb_strpos($mbReplace, ", "), // Encontre a posição da primeira ocorrência da string em uma string
    "mb_strrpos" => mb_strrpos($mbReplace, ", "), // Encontre a posição da última ocorrência de uma string em uma string
    "mb_strrpos" => mb_substr($mbReplace, 40 + 2, 14), // Pega parte da String por posicionamento e quantidade de caracteres
    "mb_strstr" => mb_strstr($mbReplace, ", ", true), // Pega a primeira ocorrência o (,) e conta antes dele
    "mb_strrchr" => mb_strrchr($mbReplace, ", ", true), // Pega a última ocorrência o (,) e conta apartir dele
]);

$mbStringReplace = $string;

echo "<p>", $mbStringReplace, "</p>";
echo "<p>", str_replace("AC/DC", "Mr. Big", $mbStringReplace), "</p>";
echo "<p>", str_replace(["AC/DC", "incrível"], "Mr. Big", $mbStringReplace), "</p>";
echo "<p>", str_replace(["AC/DC", "incrível"], ["Mr. Big", "TOP DE MAIS"], $mbStringReplace), "</p>";

$article = <<<ROCK
    <article>
        <h3>event</h3>
        <p>desc</p>
    </article>
ROCK;

$articleData = [
    "event" => "Mr. Big - Live Budokan",
    "desc" => $mbString
];

echo  str_replace(array_keys($articleData), array_values($articleData), $article);

/**
 * [ parse string ] parse_str | mb_parse_str
 */
fullStackPHPClassSession("parse string", __LINE__);

$endPoint = "name=Francisco&email=francisco.sts@hotmail.com";
$ok = mb_parse_str($endPoint, $parseEndPoint);

var_dump([
    $endPoint,
    $parseEndPoint,
    (object) $parseEndPoint
]);