<?php
require __DIR__ . '/../../fullstackphp/fsphp.php';
fullStackPHPClassName("03.12 - Cookies e sessões");

/*
 * [ cookies ] http://php.net/manual/pt_BR/features.cookies.php
 */
fullStackPHPClassSession("cookies", __LINE__);

var_dump($_COOKIE);
/* Criando o Cookie */
setcookie("estudo", "Estudo de cookie!", time() + 60);

/* Removendo o Cookie */
// setcookie('estudo', null, time() - 60);

/* Abrindo o cookie */
$cookie = filter_input_array(INPUT_COOKIE, FILTER_SANITIZE_STRIPPED);
var_dump(
    $_COOKIE,
    $cookie
);

/**
 * Intervalo de tempo 
 * TIME() = Hora atual corrente
 * 60 = 60 segundos
 * 60 * 60 = 1 hora
 * 60 * 60 * 24 = 1 dia
 * 60 * 60 * 24 * 7 = 1 Semana
 */
$time = time() + 60 * 60 * 24 * 7;

$user = [
    "user" => "root",
    "password" => "123456",
    "expire" => $time
];

setcookie(
    "login", //Nome do Cookie
    http_build_query($user), // Valor
    $time, // tempo do Cookie
    "/", // Path = Para todo o dominio
    "www.localhost", // Qual dominio ele vale
    true // TRUE = só pode ser acessado via HTTPS
);

$login = filter_input(INPUT_COOKIE, "login", FILTER_SANITIZE_STRIPPED);

if ($login) {
    var_dump($login);
    parse_str($login, $user);
    var_dump($user);
}

/*
 * [ sessões ] http://php.net/manual/pt_BR/ref.session.php
 */
fullStackPHPClassSession("sessões", __LINE__);

session_save_path(__DIR__ . "/ses");
session_name("ESTUDOSESSION");
session_start([
    "cookie_lifetime" => (60 * 60 * 24)
]);

var_dump(
    [
        "id" => session_id(),
        "name" => session_name(),
        "status" => session_status(),
        "save_path" => session_save_path(),
        "cookie" => session_get_cookie_params()
    ]
);


$_SESSION['login'] = (object) $user;
$_SESSION['user'] = $user;

// Elimina uma sessão especifica
// unset($_SESSION['user']);

// Elimina todas as sessões
session_destroy();

var_dump($_SESSION);
