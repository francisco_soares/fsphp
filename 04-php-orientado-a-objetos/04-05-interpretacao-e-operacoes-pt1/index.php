<?php
require __DIR__ . '/../../fullstackphp/fsphp.php';
fullStackPHPClassName("04.05 - Interpretação e operações pt1");

require __DIR__ . "/source/autoload.php";

/*
 * [ construct ] Executado automaticamente por meio do operador new
 * http://php.net/manual/pt_BR/language.oop5.decon.php
 */
fullStackPHPClassSession("__construct", __LINE__);

$user = new \Source\Interpretation\User("Francisco", "Assis", "francisco.sts@hotmail.com");

var_dump($user);

/*
 * [ clone ] Executado automaticamente quando um novo objeto é criado a partir do operador clone.
 * http://php.net/manual/pt_BR/language.oop5.cloning.php
 */
fullStackPHPClassSession("__clone", __LINE__);

$francisco = $user;
$isa = $francisco;

$isa->setFirstName("Isa");
$isa->setLastName("Basto");

$francisco->setFirstName("Francisco");
$francisco->setLastName("Assis");

// Clonando corretamente://
//Quando usar o CLONE, ele será pego direto no metodo __CLONE da classe USER.PHP 
$isa = clone $francisco;
$isa->setFirstName("Isa");
$isa->setLastName("Basto");

$saitama = clone $francisco;

var_dump(
    $francisco,
    $isa,
    $saitama
);

/*
 * [ destruct ] Executado automaticamente quando o objeto é finalizado
 * http://php.net/manual/pt_BR/language.oop5.decon.php
 */
fullStackPHPClassSession("__destruct", __LINE__);
