<?php

namespace Source\Contracts;

use \Source\Contracts\User;
use \Source\Contracts\UserAdmin;
use \Source\Contracts\UserInterface;

class Login
{
    private $logged;

    /**
     * Undocumented function
     *
     * @param User $user
     * @return void
     */
    public function loginUser(User $user): User
    {
        $this->logged = $user;
        return $this->logged;
    }

    /**
     * Undocumented function
     *
     * @param UserAdmin $user
     * @return void
     */
    public function loginAdmin(UserAdmin $user): UserAdmin
    {
        $this->logged = $user;
        return $this->logged;
    }

    /**
     * Undocumented function
     *
     * @param UserInterface $user
     * @return UserInterface
     */
    public function login(UserInterface $user): UserInterface
    {
        $this->logged = $user;
        return $this->logged;
    }
}
