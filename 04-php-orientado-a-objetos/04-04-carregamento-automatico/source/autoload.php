<?php

spl_autoload_register(function ($class) {
    $namespace = "Source\\";
    $baseDir = __DIR__ . "/";

    $len = strlen($namespace);

    if (strncmp($namespace, $class, $len) !== 0) {
        return;
    }

    //Retorna apartir da posição 7 de uma string
    // Ex: A variavel $class traz a string (Source\Loading\Company)
    // A variavel $len pega o tamanho da variavel $namespace = Source\ = 7, sendo assim pegara da variavel $class a partir da (Loading\Company)
    $relativeClass = substr($class, $len);

    $file = $baseDir . str_replace("\\", "/", $relativeClass) . ".php";

    if (file_exists($file)) {
        require $file;
    }
});
