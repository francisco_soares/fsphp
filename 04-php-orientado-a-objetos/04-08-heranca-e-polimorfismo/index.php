<?php

use Source\Inheritance\Event\Event;

require __DIR__ . '/../../fullstackphp/fsphp.php';
fullStackPHPClassName("04.08 - Herança e polimorfismo");

require __DIR__ . "/source/autoload.php";

/*
 * [ classe pai ] Uma classe que define o modelo base da estrutura da herança
 * http://php.net/manual/pt_BR/language.oop5.inheritance.php
 */
fullStackPHPClassSession("classe pai", __LINE__);
$event = new \Source\Inheritance\Event\Event(
    "Curso de PHP Full Stack",
    new DateTime("2019-05-20 20:30"),
    2500,
    "4"
);

var_dump($event);

$event->register("Francisco Assis", "francisco.sts@hotmail.com");
$event->register("Isa Basto", "isa@webfr.com.br");
$event->register("Alex Barros", "lex@teste.com.br");
$event->register("Gabi Passos", "gabi@teste.com.br");
$event->register("Thiago Barros", "thiago@teste.com.br");




/*
 * [ classe filha ] Uma classe que herda a classe pai e especializa seuas rotinas
 */
fullStackPHPClassSession("classe filha", __LINE__);

$address = new \Source\Inheritance\Address('Rua bem aqui perto', 1525);

$eventLive = new \Source\Inheritance\Event\EventLive(
    "Curso de PHP Full Stack",
    new DateTime("2019-05-20 20:30"),
    2500,
    "4",
    $address
);

var_dump($eventLive);

$eventLive->register("Francisco Assis", "francisco.sts@hotmail.com");
$eventLive->register("Isa Basto", "isa@webfr.com.br");
$eventLive->register("Alex Barros", "lex@teste.com.br");
$eventLive->register("Gabi Passos", "gabi@teste.com.br");
$eventLive->register("Thiago Barros", "thiago@teste.com.br");


/*
 * [ polimorfismo ] Uma classe filha que tem métodos iguais (mesmo nome e argumentos) a class
 * pai, mas altera o comportamento desses métodos para se especializar
 */
fullStackPHPClassSession("polimorfismo", __LINE__);

$eventOnline = new \Source\Inheritance\Event\EventOnline(
    "Curso de PHP Full Stack",
    new DateTime("2019-05-20 20:30"),
    97.00,
    "https://www.webfr.com.br"
);

$eventOnline->register("Francisco Assis", "francisco.sts@hotmail.com");
$eventOnline->register("Isa Basto", "isa@webfr.com.br");
$eventOnline->register("Alex Barros", "lex@teste.com.br");
$eventOnline->register("Gabi Passos", "gabi@teste.com.br");
$eventOnline->register("Thiago Barros", "thiago@teste.com.br");

