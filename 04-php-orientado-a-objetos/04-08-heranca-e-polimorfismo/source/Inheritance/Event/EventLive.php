<?php

namespace Source\Inheritance\Event;

use Source\Inheritance\Address;

class EventLive extends Event
{

    /** A Classe EventLive herda tudo da classe Event com o comando extends */
    /**
     * Undocumented variable
     *
     * @var Address
     */
    private $address;

    public function __construct($event, \DateTime $date, $price, $vacancies, Address $address)
    {
        /** Executa o construtor da classe Pai, nesse caso a classe Event */
        parent::__construct($event, $date, $price, $vacancies);

        $this->address = $address;
    }

    /**
     * Get undocumented variable
     *
     * @return  Address
     */ 
    public function getAddress(): Address
    {
        return $this->address;
    }
}
