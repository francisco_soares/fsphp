<?php

namespace Source\Inheritance\Event;

class EventOnline extends Event
{

    private $link;

    public function __construct($event, \DateTime $date, $price, $link, $vacancies = null)
    {
        parent::__construct($event, $date, $price, $vacancies);
        $this->link = $link;
    }

    /**
     * Esse método da classe é recebido da classe Event com as mesmas assinaturas, apenas trocado seu comportamento. Isso se chama polimorfismo ou override
     *
     * @param [type] $fullName
     * @param [type] $email
     * @return void
     */
    public function register($fullName, $email)
    {
        $this->vacancies += 1;
        $this->setRegister($fullName, $email);
        echo "<p class='trigger accept'>Show {$fullName}, cadastro com sucesso!</p>";
    }
}
