<?php

class User
{
    // Atributos da classe
    public $firstName;
    public $lastName;
    public $email;

    /**
     * Método da classe que retorna o nome informado.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Método da classe que seta o primeiro nome
     *
     * @param string $firstName
     * @return void
     */
    public function setFirstName($firstName)
    {
        $this->firstName = filter_var($firstName, FILTER_SANITIZE_STRING);
    }

    /**
     * Método da classe que retorna o sobrenome informado
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Método da classe que seta o sobrenome
     *
     * @param string $lastName
     * @return void
     */
    public function setLastName($lastName)
    {
        $this->lastName = filter_var($lastName, FILTER_SANITIZE_STRIPPED);
    }

    /**
     * Método da classe que retorna o e-mail
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Valida o e-mail do usuário em um formato válido
     *
     * @param $email
     * @return bool
     */
    public function setEmail($email)
    {
        $this->email = $email;

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }
}
