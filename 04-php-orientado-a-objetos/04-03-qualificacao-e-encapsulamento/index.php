<?php
require __DIR__ . '/../../fullstackphp/fsphp.php';
fullStackPHPClassName("04.03 - Qualificação e encapsulamento");

/*
 * [ namespaces ] http://php.net/manual/pt_BR/language.namespaces.basics.php
 */
fullStackPHPClassSession("namespaces", __LINE__);

require __DIR__ . "/classes/App/Template.php";
require __DIR__ . "/classes/Web/Template.php";

$appTemplate = new App\Template();
$webTemplate = new Web\Template();

var_dump(
    $appTemplate,
    $webTemplate
);

use App\Template;
use Source\Qualifield\User;
use Web\Template AS webTemplate;

$appUserTemplate = new Template();
$webUserTemplate = new webTemplate();

var_dump(
    $appUserTemplate,
    $webUserTemplate
);

/*
 * [ visibilidade ] http://php.net/manual/pt_BR/language.oop5.visibility.php
 */
fullStackPHPClassSession("visibilidade", __LINE__);

require __DIR__ . "/source/QualiField/User.php";

$user = new \Source\Qualifield\User;

// Os atributos da classe já estão definidos como PRIVATE.
// Sendo assim, se eu tentar acessar da forma abaixo, vai apresentar um erro
// $user->firstName = "Francisco";
// $user->lastName = "Assis";

// Como os métodos da classes estão como PRIVADO, não conseguirei acessar da forma abaixo.
// $user->setFirstName("Francisco");
// $user->setLastName("Assis");

var_dump(
    $user,
    get_class_methods($user)
);

/*
 * [ manipulação ] Classes com estruturas que abstraem a rotina de manipulação de objetos
 */
fullStackPHPClassSession("manipulação", __LINE__);

$francisco = $user->setUser("Francisco", "Assis", "contato@webfr.com.br");

if(!$francisco){
    echo "<p class='trigger error'>{$user->getError()}</p>";
}

$isa = new \Source\Qualifield\User;
$isa->setUser("Isa", "Basto", "isa@webfr.com.br");

var_dump(
    $user,
    $isa
);
