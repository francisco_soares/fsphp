<?php

namespace Source\Related;

class Address
{
    private $street;
    private $number;
    private $complements;

    public function __construct($street, $number, $complements)
    {
        $this->street = $street;
        $this->number = $number;
        $this->complements = $complements;
    }

    /**
     * Get the value of street
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Get the value of number
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Get the value of complements
     */
    public function getComplements()
    {
        return $this->complements;
    }
}
