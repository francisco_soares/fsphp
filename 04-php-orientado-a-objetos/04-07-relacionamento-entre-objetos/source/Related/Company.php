<?php

namespace Source\Related;

use Source\Related\User as RelatedUser;
use User;

class Company
{
    private $company;
    /**
     *Estou associando a variavel $address com o Address da clase no método boot()
     * @var Address
     */
    private $address;
    private $team;
    private $products;

    public function bootCompany($company, $address)
    {
        $this->company = filter_var($company, FILTER_SANITIZE_STRIPPED);
        $this->address = $address; 
    }

    /**
     * Estou associando o ADDRESS a classe Address.php. Onde deve ser passado nesse parametro uma estancia da classe.
     *
     * @param [type] $company
     * @var Address $address = 
     * @return void
     */
    public function boot($company, Address $address)
    {
        $this->company = filter_var($company, FILTER_SANITIZE_STRIPPED);
        $this->address = $address;
    }

    /**
     * Aqui estou fazendo uma agregação na variavel $product com a classe Product
     *
     * @param Product $product
     * @return void
     */
    public function addProduct(Product $product)
    {
        $this->products[] = $product;
    }

    public function addTeamMenber($job, $firstName, $lastName)
    {   
        $this->team[] = new \Source\Related\User($job, $firstName, $lastName);
    }

    /**
     * Get the value of company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * [AVISO] - Eu posso usar o @return Adress abaixo ou do lado do método
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * Get the value of team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }
}
