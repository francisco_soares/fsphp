<?php

use Source\Traits\Cart;

require __DIR__ . '/../../fullstackphp/fsphp.php';
fullStackPHPClassName("04.12 - Comportamentos com traits");

require __DIR__ . "/source/autoload.php";

/*
 * [ trait ] São traços de código que podem ser reutilizados por mais de uma classe. Um trait é como um compoetamento
 * do objeto (BEHAVES LIKE). http://php.net/manual/pt_BR/language.oop5.traits.php
 */
fullStackPHPClassSession("trait", __LINE__);

$user = new \Source\Traits\User("Francisco", "Assis", "francisco.sts@hotmail.com");
$address = new \Source\Traits\Address("Rua bem perto", 150, "Casa 50");
$register = new \Source\Traits\Register($user, $address);

var_dump(
    $user,
    $address,
    $register,
    $register->getUser(),
    $register->getAddress(),
    $register->getUser()->getFirstName(),
    $register->getAddress()->getStreet()
);

$cart = new \Source\Traits\Cart();
$cart->add(1, "Curso de PHP Avançado", 1, 2000);
$cart->add(2, "Curso de PHP Avançado", 2, 1000);
$cart->add(3, "Curso de Javascript Avançado", 5, 500);
$cart->remove(2, 1);
$cart->remove(3, 5);

$cart->checkout($user, $address);


var_dump($cart);
