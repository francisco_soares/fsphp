<?php

namespace Source\Traits;

use Source\Traits\Address;

trait AddressTrait
{
    private $address;

    /**
     * Undocumented function
     *
     * @return Address
     */ 
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * Set the value of address
     *
     * @return  self
     */ 
    public function setAddress(Address $address): void
    {
        $this->address = $address;
    }
}
