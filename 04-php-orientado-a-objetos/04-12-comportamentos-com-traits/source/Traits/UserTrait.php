<?php

namespace Source\Traits;

use Source\Traits\User;

trait UserTrait
{
    private $user;

    /**
     * Undocumented function
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @return  self
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }
}
