<?php

namespace Source\Members;

class Config
{
    /** Constantes */
    public const COMPANY = "Web-Fr";
    protected const DOMAIN = "webfr.com.br";
    private const SECTOR = "Digital";

    public static $company;
    public static $domain;
    public static $sector;

    public static function setConfig($company, $domain, $sector)
    {
        self::$company = $company;
        self::$domain = $domain;
        self::$sector = $sector;
    }
}
